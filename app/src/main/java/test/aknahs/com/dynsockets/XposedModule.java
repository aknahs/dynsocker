package test.aknahs.com.dynsockets;

import android.app.AlarmManager;
import android.util.Log;

import java.lang.reflect.Method;
import java.net.Socket;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import test.aknahs.com.proxies.SocketProxy;
import test.aknahs.com.proxies.ThreadProxy;

/**
 * The Xposed Module.
 * The handleLoadPackage is invoked when the application starts
 */
public class XposedModule implements IXposedHookLoadPackage {

    public final static String TAG = "DynSockets";

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {

        if (!loadPackageParam.packageName.contains("facebook"))
            return;

        instrumentClass(loadPackageParam, AlarmManager.class, null);
        instrumentClass( loadPackageParam, Socket.class, SocketProxy.class);
        instrumentThreads(loadPackageParam, Thread.class, ThreadProxy.class);
    }

    private void instrumentClass(XC_LoadPackage.LoadPackageParam loadPackageParam, Class<?> cls, Class<?> proxy){
        Log.v(TAG, "Instrumenting " + cls.getSimpleName() + " of application : " + loadPackageParam.packageName);

        /*Replace the Socket by our SocketProxy class.*/
        XposedBridge.hookAllConstructors(cls, new ProxyHook(proxy));

        /*Hook all declared methods from the Sockets*/
        for (Method m : cls.getDeclaredMethods()) {
            /*We use the objects toString, if it is hooked there will be infinite recursion :)*/
            if(m.getName().contains("toString"))
                continue;
            Log.v(XposedModule.TAG, "Hooking Method : " + m.toGenericString());
            XposedBridge.hookMethod(m, new ProxyHook(proxy));
        }
    }

    private void instrumentThreads(XC_LoadPackage.LoadPackageParam loadPackageParam, Class<?> cls, Class<?> proxy){
        Log.v(TAG, "Instrumenting " + cls.getSimpleName() + " of application : " + loadPackageParam.packageName);

        /*Hook all declared methods from the Sockets*/
        for (Method m : cls.getDeclaredMethods()) {
            /*We use the objects toString, if it is hooked there will be infinite recursion :)*/
            if(m.getName().contains("toString"))
                continue;

            /*This suffices since we get the beggining and end of run :)*/
            if(m.getName().contains("run")) {
                Log.v(XposedModule.TAG, "Hooking Method : " + m.toGenericString());
                XposedBridge.hookMethod(m, new ProxyHook(proxy));
            }
        }
    }
}
