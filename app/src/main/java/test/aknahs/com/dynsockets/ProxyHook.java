package test.aknahs.com.dynsockets;

import android.util.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Random;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;

/**
 * Redirects socket methods through our SocketProxy.
 */
public class ProxyHook extends XC_MethodHook {

    private Method _method = null;
    private Random r = new Random();
    private Class<?> proxyClass = null;

    public ProxyHook(Class<?> proxy) {
        super();
        proxyClass = proxy;
    }

    /**
     * Returns a String representation of the object types and values
     *
     * @param args  Object values
     * @param types Types of objects
     * @return string representation
     */
    private String argumentsToString(Object[] args, Class<?>[] types) {
        String arguments = "";

        for (int i = 0; i < args.length; i++) {
            /*We assume, by looking at possible socket arguments, that their parameters ToString can be invoked*/
            arguments += types[i].getName() + ":" + ((args[i] != null) ? args[i].toString() : "null") + ",";
        }

        if (args.length > 0)
            return arguments.substring(0, arguments.length() - 1);
        else return "";
    }

    @Override
    protected final void beforeHookedMethod(MethodHookParam param) throws Throwable {

        Integer id = r.nextInt();
        param.setObjectExtra("id", id);
        Boolean isConstructor = Constructor.class.isAssignableFrom(param.method.getClass());
        Class<?>[] types;

        if (isConstructor)
            types = ((Constructor<?>) param.method).getParameterTypes();
        else
            types = ((Method) param.method).getParameterTypes();

        String methodLog = "[" + id + "]" + "[" + (param.thisObject == null ? "" : param.thisObject.getClass().getName()) + "] "
                + Thread.currentThread().getName() + " invoked -> " + Modifier.toString(param.method.getModifiers()) + " " +
                param.method.getName() + "(" + argumentsToString(param.args, types) + ")";


        Log.v(XposedModule.TAG, methodLog);

        /*If a proxy class was defined when the ProxyHook was constructed*/
        if (proxyClass != null) {
        /*There is ProxyHook instance per method, so we can actually store the method to invoke*/
            if (_method == null) {
            /*find the correspondent method of the proxy class*/
                try {
                    if (isConstructor)
                        _method = XposedHelpers.findMethodExact(proxyClass,
                                "constructor",
                                ((Method) param.method).getParameterTypes());
                    else
                        _method = XposedHelpers.findMethodExact(proxyClass,
                                param.method.getName(),
                                ((Method) param.method).getParameterTypes());

                } catch (Throwable t) {

                    if (isConstructor)
                        Log.v(XposedModule.TAG, "SocketProxy did not contain constructor : " + ((Constructor<?>) param.method).toGenericString());
                    else
                        Log.v(XposedModule.TAG, "SocketProxy did not contain method : " + ((Method) param.method).toGenericString());
                    return;
                }
            }

            /*Invoke through our SocketProxy respective method*/
            Object ret = _method.invoke(null, param.args);
        }


    }

    @Override
    protected void afterHookedMethod(MethodHookParam param) throws Throwable {

        String methodLog;
        Integer id = (Integer) param.getObjectExtra("id");
        Boolean isConstructor = Constructor.class.isAssignableFrom(param.method.getClass());
        Class<?>[] types = null;

        if (isConstructor)
            types = ((Constructor<?>) param.method).getParameterTypes();
        else
            types = ((Method) param.method).getParameterTypes();

        if (id == null)
            Log.v(XposedModule.TAG, "------------------------>>> null id!!!");

         /*After method execution-----------*/
        methodLog = "[" + id + "]" + "[" + (param.thisObject == null ? "" : param.thisObject.getClass().getName()) + "] "
                + Thread.currentThread().getName() + " returned -> " + Modifier.toString(param.method.getModifiers()) + " " +
                param.method.getName() + "(" + argumentsToString(param.args, types) + "):" +
                (Constructor.class.isAssignableFrom(param.method.getClass()) ? "" : (param.getResult() == null ? "null" : param.getResult().toString()));

        Log.v(XposedModule.TAG, methodLog);
    }
}
