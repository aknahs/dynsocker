package test.aknahs.com.proxies;

/**
 * Created by aknahs on 10/04/15.
 */
public class ThreadProxy{
    public static void constructor() {
        //super();
    }

    public static void constructor(Runnable runnable) {
        //super(runnable);
    }

    public static void constructor(Runnable runnable, String threadName) {
        //super(runnable, threadName);
    }

    public static void constructor(String threadName) {
        //super(threadName);
    }

    public static void constructor(ThreadGroup group, Runnable runnable) {
        //super(group, runnable);
    }

    public static void constructor(ThreadGroup group, Runnable runnable, String threadName) {
        //super(group, runnable, threadName);
    }

    public static void constructor(ThreadGroup group, String threadName) {
        //super(group, threadName);
    }

    public static void constructor(ThreadGroup group, Runnable runnable, String threadName, long stackSize) {
        //super(group, runnable, threadName, stackSize);
    }

    
    public static int countStackFrames() {
        return 0;//super.countStackFrames();
    }

    
    public static void destroy() {
        //super.destroy();
    }

    
    public static ClassLoader getContextClassLoader() {
        return null;//super.getContextClassLoader();
    }

    
    public static long getId() {
        return 0;//super.getId();
    }

    
    public static StackTraceElement[] getStackTrace() {
        return null;//super.getStackTrace();
    }

    
    public static Thread.State getState() {
        return null;//super.getState();
    }

    
    public static Thread.UncaughtExceptionHandler getUncaughtExceptionHandler() {
        return null;//super.getUncaughtExceptionHandler();
    }

    
    public static void interrupt() {
        //super.interrupt();
    }

    
    public static boolean isInterrupted() {
        return false;//super.isInterrupted();
    }

    
    public static void run() {
        //super.run();
    }

    
    public static void setContextClassLoader(ClassLoader cl) {
        //super.setContextClassLoader(cl);
    }

    
    public static void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler handler) {
        //super.setUncaughtExceptionHandler(handler);
    }

    
    public static /*synchronized*/ void start() {
        //super.start();
    }
}
