package test.aknahs.com.proxies;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketImpl;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;


/**
 * Class that is invoked instead of the original Socket.
 * Had the logging stuff in this class.
 */
public class SocketProxy{

   public static void constructor() {

    }

   public static void constructor(Proxy proxy) {

    }

   public static void constructor(String dstName, int dstPort) throws UnknownHostException, IOException {

    }

   public static void constructor(String dstName, int dstPort, InetAddress localAddress, int localPort) throws IOException {

    }

   public static void constructor(String hostName, int port, boolean streaming) throws IOException {

    }

   public static void constructor(InetAddress dstAddress, int dstPort) throws IOException {

    }

   public static void constructor(InetAddress dstAddress, int dstPort, InetAddress localAddress, int localPort) throws IOException {

    }

   public static void constructor(InetAddress addr, int port, boolean streaming) throws IOException {

    }

    protected void constructor(SocketImpl impl) throws SocketException {

    }

   public static synchronized void close() throws IOException {
        //Log.v(XposedModule.TAG, "Socket closed!");
    }

    
   public static InetAddress getInetAddress() {
        return null; //super.getInetAddress();
    }

    
   public static InputStream getInputStream() throws IOException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getInputStream");
        return null; //super.getInputStream();
    }

    
   public static boolean getKeepAlive() throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getKeepAlive");
        return false; //super.getKeepAlive();
    }

    
   public static InetAddress getLocalAddress() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getLocalAddress");
        return null; //super.getLocalAddress();
    }

    
   public static int getLocalPort() {
        //Log.v(XposedModule.TAG, "Intercepted socket method : getLocalPort");
        return 0; //super.getLocalPort();
    }

    
   public static OutputStream getOutputStream() throws IOException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getOutputStream");
        return null; //super.getOutputStream();
    }

    
   public static int getPort() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getPort");
        return 0; //super.getPort();
    }

    
   public static int getSoLinger() throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getSoLinger");
        return 0; //super.getSoLinger();
    }

    
   public static synchronized int getReceiveBufferSize() throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getReceiveBufferSize");
        return 0; //super.getReceiveBufferSize();
    }

    
   public static synchronized int getSendBufferSize() throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getSendBufferSize");
        return 0; //super.getSendBufferSize();
    }

    
   public static synchronized int getSoTimeout() throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getSoTimeout");
        return 0; //super.getSoTimeout();
    }

    
   public static boolean getTcpNoDelay() throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getTcpNoDelay");
        return false; //super.getTcpNoDelay();
    }

    
   public static void setKeepAlive(boolean keepAlive) throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setKeepAlive("+keepAlive+")");
        //super.setKeepAlive(keepAlive);
    }

    
   public static synchronized void setSendBufferSize(int size) throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setSendBufferSize(" + size +")");
        //super.setSendBufferSize(size);
    }

    
   public static synchronized void setReceiveBufferSize(int size) throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setReceiveBufferSize");
        //super.setReceiveBufferSize(size);
    }

    
   public static void setSoLinger(boolean on, int timeout) throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setSoLinger");
        //super.setSoLinger(on, timeout);
    }

    
   public static synchronized void setSoTimeout(int timeout) throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setSoTimeout");
        //super.setSoTimeout(timeout);
    }

    
   public static void setTcpNoDelay(boolean on) throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setTcpNoDelay");
        //super.setTcpNoDelay(on);
    }

    
   public static void shutdownInput() throws IOException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: shutdownInput");
        //super.shutdownInput();
    }

    
   public static void shutdownOutput() throws IOException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: shutdownOutput");
        //super.shutdownOutput();
    }

    
   public static SocketAddress getLocalSocketAddress() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getLocalSocketAddress");
        return null; //super.getLocalSocketAddress();
    }

    
   public static SocketAddress getRemoteSocketAddress() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getRemoteSocketAddress");
        return null; //super.getRemoteSocketAddress();
    }

    
   public static boolean isBound() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: isBound");
        return false; //super.isBound();
    }

    
   public static boolean isConnected() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: isConnected");
        return false; //super.isConnected();
    }

    
   public static boolean isClosed() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: isClosed");
        return false; //super.isClosed();
    }

    
   public static void bind(SocketAddress localAddr) throws IOException {
        //Log.v(XposedModule.TAG, "Socket bind!");
        //super.bind(localAddr);
    }

    
   public static void connect(SocketAddress remoteAddr) throws IOException {
        //Log.v(XposedModule.TAG, "Socket connect!");
        //super.connect(remoteAddr);
    }

    
   public static void connect(SocketAddress remoteAddr, int timeout) throws IOException {
        //Log.v(XposedModule.TAG, "Socket connect!");
        //super.connect(remoteAddr, timeout);
    }

    
   public static boolean isInputShutdown() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: isInputShutdown");
        return false; //super.isInputShutdown();
    }

    
   public static boolean isOutputShutdown() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: isOutputShutdown");
        return false; //super.isOutputShutdown();
    }

    
   public static void setReuseAddress(boolean reuse) throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setReuseAddress");
        //super.setReuseAddress(reuse);
    }

    
   public static boolean getReuseAddress() throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getReuseAddress");
        return false; //super.getReuseAddress();
    }

    
   public static void setOOBInline(boolean oobinline) throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setOOBInline");
        //super.setOOBInline(oobinline);
    }

    
   public static boolean getOOBInline() throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getOOBInline");
        return false; //super.getOOBInline();
    }

    
   public static void setTrafficClass(int value) throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setTrafficClass");
        //super.setTrafficClass(value);
    }

    
   public static int getTrafficClass() throws SocketException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getTrafficClass");
        return 0; //super.getTrafficClass();
    }

    
   public static void sendUrgentData(int value) throws IOException {
        //Log.v(XposedModule.TAG, "Intercepted socket method: sendUrgentData");
        //super.sendUrgentData(value);
    }

    
   public static SocketChannel getChannel() {
        //Log.v(XposedModule.TAG, "Intercepted socket method: getChannel");
        return null; //super.getChannel();
    }

    
   public static void setPerformancePreferences(int connectionTime, int latency, int bandwidth) {
        //Log.v(XposedModule.TAG, "Intercepted socket method: setPerformancePreferences");
        //super.setPerformancePreferences(connectionTime, latency, bandwidth);
    }
}
