package test.aknahs.com.proxies;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.util.Log;

import test.aknahs.com.dynsockets.XposedModule;

/**
 * Created by aknahs on 15/05/15.
 */
public class AlarmManagerProxy{
    
    public static void set(int type, long triggerAtMillis, PendingIntent operation) {
        Log.v(XposedModule.TAG, "AlarmManager,set,type-"+type+",triggetAtMillis-"+triggerAtMillis);
       //supper.set(type, triggerAtMillis, operation);
    }


    public static void setRepeating(int type, long triggerAtMillis, long intervalMillis, PendingIntent operation) {
        Log.v(XposedModule.TAG, "AlarmManager,set,type-"+type+",triggetAtMillis-"+triggerAtMillis+",intervalMillis-"+intervalMillis);
       //supper.setRepeating(type, triggerAtMillis, intervalMillis, operation);
    }


    public static void setWindow(int type, long windowStartMillis, long windowLengthMillis, PendingIntent operation) {
        Log.v(XposedModule.TAG, "AlarmManager,set,type-"+type+",windowStartMillis-"+windowStartMillis+",windowLengthMillis-"+windowLengthMillis);
       //supper.setWindow(type, windowStartMillis, windowLengthMillis, operation);
    }


    public static void setExact(int type, long triggerAtMillis, PendingIntent operation) {
        Log.v(XposedModule.TAG, "AlarmManager,set,type-"+type+",triggetAtMillis-"+triggerAtMillis);
       //supper.setExact(type, triggerAtMillis, operation);
    }


    public static void setAlarmClock(AlarmManager.AlarmClockInfo info, PendingIntent operation) {
       //supper.setAlarmClock(info, operation);
    }


    public static void setInexactRepeating(int type, long triggerAtMillis, long intervalMillis, PendingIntent operation) {
       //supper.setInexactRepeating(type, triggerAtMillis, intervalMillis, operation);
    }


    public static void cancel(PendingIntent operation) {
       //supper.cancel(operation);
    }


    public static void setTime(long millis) {
       //supper.setTime(millis);
    }


    public static void setTimeZone(String timeZone) {
       //supper.setTimeZone(timeZone);
    }

    
    public static void getNextAlarmClock() {
        return;//supper.getNextAlarmClock();
    }
}
